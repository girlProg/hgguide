import React, { useState } from "react";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import OcticonsIcon from "react-native-vector-icons/Octicons";
import { setNavigator } from './src/navigationRef';
import SimpleLineIconsIcon from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIconsIcon from "react-native-vector-icons/MaterialCommunityIcons";

import { AppLoading } from "expo";
import * as Font from "expo-font";
import LandingPage from "./src/screens/LandingPage";
import Untitled1 from "./src/screens/Untitled1";
import SelectLocation from "./src/screens/SelectLocation";
import ConfirmLocation from "./src/screens/ConfirmLocation";
import SelectDate from "./src/screens/SelectDate";
// import Untitled4f from "./src/screens/Untitled4f";
import EventDetail from "./src/screens/EventDetail";
import CountdownScreen from "./src/screens/CountdownScreen";
import LogoTitle from './src/components/LogoTitle';
import { Dimensions } from 'react-native';
import { Provider  } from './src/context/eventsContext';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';



// const DrawerNavigation = createDrawerNavigator({
//   EventDetail: EventDetail,

//   SelectDate: SelectDate,
//   SelectLocation: SelectLocation,
//   Untitled1: Untitled1,
//   LandingPage: LandingPage,

// const DrawerNavigation = createDrawerNavigator({
//   SelectLocation: SelectLocation,
//   EventDetail: EventDetail,

//   SelectDate: SelectDate,
  
//   Untitled1: Untitled1,
//   LandingPage: LandingPage,
//   // ConfirmLocation: ConfirmLocation,
  
//   // Untitled4f: Untitled4f,
//   Untitled6: Untitled6,
//   // Untitled7: Untitled7
// });

// const StackNavigation = createStackNavigator(
//   {
//     DrawerNavigation: {
//       screen: DrawerNavigation
//     },
//     Untitled1: Untitled1,
//     LandingPage: LandingPage,
    
//     SelectLocation: SelectLocation,
//     ConfirmLocation: ConfirmLocation,
//     SelectDate: SelectDate,
//     // Untitled4f: Untitled4f,
//     EventDetail: EventDetail,
//     Untitled6: Untitled6,
//     // Untitled7: Untitled7
//   },
//   {
//     defaultNavigationOptions:{
//       headerTitle: <LogoTitle />,
//     headerTintColor: 'white',
//     headerStyle: {
//       // backgroundColor: '#131416',
//       height : Dimensions.get('window').height/12,
//     },
//   // headerLeft: <BackButton/> 
//     headerBackTitle: 'abck '
//     }
//   }
// );

const {height, width} = Dimensions.get('window')

const stackNavOptions = 
{
  defaultNavigationOptions: {
    headerTitle: <LogoTitle />,
    headerTintColor: 'black',
    headerStyle: {
    // backgroundColor: '#131416',
    height : Dimensions.get('window').height/12,
  },
// headerLeft: <BackButton/> 
  // headerBackTitle: 'abck '
} 
  
}


const navigator = createBottomTabNavigator({
  TabA: createStackNavigator({
    // a: LocalNotications,
    TabA: SelectLocation,
    EventDetail: EventDetail
    },
    {
      ...stackNavOptions,
      mode: "modal",
      // headerMode: 'none',
    }
  ), 
  TabB: createStackNavigator({
    TabB: SelectDate,
    EventDetail: EventDetail
  },
  {
    ...stackNavOptions
  }
  ),
  TabC: createStackNavigator({
    TabC: CountdownScreen,
    EventDetail: EventDetail
  },
  {
    ...stackNavOptions
  }
  ) ,
},
{
  // tabBarComponent: Tabbar,
  defaultNavigationOptions: ({navigation}) =>({
    tabBarIcon: ({focused, horizontal, tintColor }) => {
      const {routeName} = navigation.state
      color_name = focused ? 'white' :  'rgba(0,0,0,1)' 
      let iconSize = 35
      if (routeName === 'TabA'){
        return <OcticonsIcon name = 'home' size = {iconSize} color= {color_name}></OcticonsIcon>
      }else if (routeName === 'TabB'){
        return <SimpleLineIconsIcon name="calendar" size = {iconSize} color= {color_name} ></SimpleLineIconsIcon>
      }else if (routeName === 'TabC'){
        return <MaterialCommunityIconsIcon name="circle-slice-5" size = {iconSize} color = {color_name}></MaterialCommunityIconsIcon>
      }
    },
  
}),


backBehavior: 'history',
tabBarOptions: {
    showLabel: false,
    keyboardHidesTabBar: false,
    tabStyle: {
      // marginTop: 1,
      // height: height /6,
      backgroundColor: '#e6e6e6'
    },
    style: {
      height: height /12,
      backgroundColor: '#e6e6e6',
    },
}
}
);


const HgApp = createAppContainer(navigator);

export default () => {
    const [isLoadingComplete, setLoadingComplete] = useState(false);
  if (!isLoadingComplete) {
    return (
      <Provider>
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
      </Provider>
    );
  } else {
    return <Provider>
        <HgApp />
    </Provider>
     
  }
}
async function loadResourcesAsync() {
  await Promise.all([
    Font.loadAsync({
      "fugaz-one-regular": require("./src/assets/fonts/fugaz-one-regular.ttf"),
      "roboto-regular": require("./src/assets/fonts/roboto-regular.ttf"),
      "Futura-Medium": require("./src/assets/fonts/futura-medium.ttf"),
      "FuturaBT-Medium": require("./src/assets/fonts/futura-medium.ttf"),
      "Futura-book": require("./src/assets/fonts/futura-book.ttf"),
    })
  ]);

  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      alert('Please allow notifications in settings to allow for reminders for events you are interested in');
      return;
    }
    token = await Notifications.getExpoPushTokenAsync();
    // console.log(token);
    // setExpoPushToken(token)
  } else {
    // alert('Must use physical device for Push Notifications');
  }
}
function handleLoadingError(error) {
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

