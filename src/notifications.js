import React, {useState, useEffect} from 'react';
import { Text, View, Button, Vibration, Platform } from 'react-native';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';


const LocalNotications = ({}) => {
    const [expoPushToken, setExpoPushToken] = useState('')
    const [notification, setNotification] = useState('')
    useEffect (()=>{ registerForPushNotificationsAsync();  },[])
    const threeHoursInMilliseconds = 1.08e+7
    const oneSecond = 1000



  scheduleNotification = async (event) => {
    await registerForPushNotificationsAsync()
  }

  const registerForPushNotificationsAsync = async (event) => {
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = await Notifications.getExpoPushTokenAsync();
      console.log(token);
      setExpoPushToken(token)
    } else {
      alert('Must use physical device for Push Notifications');
    }

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }else{
        Notifications.scheduleLocalNotificationAsync({title: event.name, body: `event is about to start at ${new Date(event.date)}` ,ios: {_displayInForeground: true} }, {time: new Date(event.date) - threeHoursInMilliseconds})
        Notifications.scheduleLocalNotificationAsync({title: event.name, body: `You will be notified 3hours before event starts on the day!` ,ios: {_displayInForeground: true} }, {time: new Date(event.date) - oneSecond})
    }
  };

  _handleNotification = notification => {
    Vibration.vibrate();
    console.log(notification);
    setNotification(notification)
};

    return (
        <View
        style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'space-around',
        }}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text>Origin: {notification.origin}</Text>
            <Text>Data: {JSON.stringify(notification.data)}</Text>
        </View>
        <Button title={'Press to Send Notification'} onPress={() => registerForPushNotificationsAsync()} />
        </View>
    );
  
}

export default LocalNotications