import React, { useState, useContext, useEffect } from "react";
import { StyleSheet, View, TouchableOpacity, Text, Dimensions, ActivityIndicator, Platform} from "react-native";
import EntypoIcon from "react-native-vector-icons/Entypo";
import ModalSelector from 'react-native-modal-selector'
import Swiper from 'react-native-deck-swiper'
import {Context} from '../context/eventsContext'
import ProgressiveImage from '../components/ProgressiveImage';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'
import AdvertPanel from "../components/AdvertPanel";
import { Notifications } from 'expo';
import LocalNotications from '../Notifications';


const {height, width} = Dimensions.get('window')
const cardWidth = width-100

const SelectLocation = ({navigation}) => {
  const { state, getEvents, getStates, markAttendance, getAttendanceForPhone} = useContext(Context)
  const [shouldLoad, setShouldLoad] = useState(null) 
  const [showBar, setShowBar] = useState(false) 
  const [showGoing, setShowGoing] = useState(false) 
  const [showNot, setShowNot] = useState(false) 
  const [cardSwiper, setCardSwiper] = useState({}) 
  const [previousEvent, setPreviousEvent] = useState(-1) 
  const [isLastCard, setIsLastCard] = useState(false) 
  const [selectedState, setSelectedState] = useState(false) 
  const [currentAttendance , setCurrentAttendance] = useState(false) 
  
  const thumb = 'https://img.imageboss.me/width/200/quality:40/'
  const [expoPushToken, setExpoPushToken] = useState('')
  const [notification, setNotification] = useState('')
  const threeHoursInMilliseconds = 1.08e+7
  const oneSecond = 1000

  useEffect (()=>{
    getAttendanceForPhone(); 
    getEvents(); 
    getStates(); 
    // state && state.events && setCurrentAttendance(state.attendanceForPhone.find((attend)=>{return attend.event == state.events[0].id}))
    // state && state.events && setShowGoing (currentAttendance? currentAttendance.attend? true : false : true )
    // state && state.events && setShowNot (currentAttendance? currentAttendance.attend ? false : true : true)
    // state && state.events && setShowBar(showGoing && showNot) 
  },[showGoing, showNot, cardSwiper])

  //the height and width of device in use. this is what i use to make designs responsive


  const registerEventGoing = async () =>{
    cardSwiper.swipeLeft()
    const currentEvent = previousEvent == -1 ? state.events[0] : state.events[previousEvent+1]
    await scheduleNotification(currentEvent)
    await markAttendance(currentEvent, true)
    // cardSwiper.jumpToCardIndex(previousEvent+1)
  }
  
  const registerEventNot = async () =>{
    //animate a swipe
    cardSwiper.swipeRight()
    
    //a check to see if its the first card or not
    const currentEvent = previousEvent == -1 ? state.events[0] : state.events[previousEvent+1]
    await scheduleNotification(currentEvent)
    //attendance is stored remotely. each installation of the app on a device will have its unique app id
    await markAttendance(currentEvent, false)
    

  }

  const scheduleNotification = async (event) => {
    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }else{
      // alert(`${event.name}   ${new Date(event.date).getTime()}  ${new Date().getTime()}`)
        Notifications.scheduleLocalNotificationAsync({title: event.name, body: `Is about to start in three hours at: ${event.address} : ${new Date(event.date)}` ,ios: {_displayInForeground: true} }, {time: new Date(event.date).getTime() + oneSecond})
        Notifications.scheduleLocalNotificationAsync({title: event.name, body: `You will be notified 3 hours before the event starts on the day: ${new Date(event.date).getTime()}` ,ios: {_displayInForeground: true} }, {time: new Date().getTime() + oneSecond})
    }
  }

  const reloadEvents = async (cardIndex) =>{
    console.log(previousEvent)
    setIsLastCard(false)
    getEvents()
    setSelectedState('Choose')
    setPreviousEvent(-1)
    setShowGoing(null)
    setShowNot(null)
  }


  const handleSwiped = async (cardIndex) =>{
    //plus two
    if(cardIndex+2 <= state.events.length){
      // console.log(cardIndex);
      setPreviousEvent(cardIndex); 
      //get the attendance object for the current event in view
      const attendanceForProject = state.attendanceForPhone.find((attend)=>{return attend.event == state.events[cardIndex+1].id; console.log(attend.event + ' ' + state.events[cardIndex+1].id)})
      //set the opposite value of the attendance to determine if the button should be shown or not.
      // if the attendance doesnt exist at all, show both
      var foundEvent = attendanceForProject ?true : false
      console.log(foundEvent)
      // console.log(foundEvent? attendanceForProject.attend? true : false : true )
      // console.log(foundEvent?  )
      // setShowGoing(true)
      setShowGoing(foundEvent ? attendanceForProject.attend? true : false : true)
      console.log(showGoing)
      setShowNot (foundEvent? attendanceForProject.attend ? false : true : true)
      console.log(showNot)
      //usnig logic, only show the bar between going and not only if you are shoing both buttons
      
      setShowBar(showGoing === true ? showNot === true ? true : false : false)
      //cumbersome - should delete
      // setShowBar(foundEvent? attendanceForProject.attend ? false : true : true === true ? foundEvent? attendanceForProject.attend ? false : true : true === true ? true : false : false )
      console.log(showGoing == true ? showNot == true ? true : false : false)
      // console.log(showBar)
      
    }
    
  }

  return (
    <View style={styles.container}>
      <View style={styles.citySelector  }>
        <ModalSelector
            data={state? state.states? state.states : [] : []}
            initValue="Select something yummy!"
            onChange={(option)=>{setSelectedState(option.id); getEvents(selectedState); setIsLastCard(false)}}
          >
            <TouchableOpacity style={styles.button4}>
                <View style={styles.icon5Row}>
                  <EntypoIcon name="location" style={styles.icon5}></EntypoIcon>
                  <Text style={styles.selectHere}>{selectedState ? selectedState : 'Choose State'}</Text>
                </View>
              </TouchableOpacity>
 
                </ModalSelector>

      </View>

        <View style={{ textAlignVertical: 'center', textAlign:'center', alignContent:'center' , justifyContent: 'center', height: width+width/7}}>
        {
          state.events && !isLastCard ?
          state.events.length > 0 ?
            <Swiper  
                ref={swiper => { setCardSwiper(swiper) }} 
                onTapCard={(card)=>{navigation.navigate('EventDetail', {event: state.events[card]})}}
                cards={state? state.events? state.events : [] : []}
                renderCard={(card) => {
                    return (
                        <View onPress={()=>{navigation.navigate('EventDetail', card); }} style={styles.rect111}>
                            {/* https://medium.com/react-native-training/progressive-image-loading-in-react-native-e7a01827feb7 */}
                            <ProgressiveImage
                              thumbnailSource={card &&{ uri: thumb+card.images[0].image }}
                              style={{ height: cardWidth, width: cardWidth, resizeMode: "cover", justifyContent:'center', borderRadius: 17 }}
                              source={card && {uri: card.images[0].image} } />
                        </View>
                    )
                }}
                onSwiped={(cardIndex) => {handleSwiped(cardIndex)}}
                onSwipedAll={() => {setIsLastCard(true);}}
                cardIndex={0}
                infinite={false}
                backgroundColor={'#fff'}
                stackSize= {2}>
                
            </Swiper>
          :
          <View> 
            <Text style={styles.noEvents}>No events for location</Text>  
              <TouchableOpacity onPress={()=>reloadEvents()} style={styles.reloadButtonContainer}>
                <Text style={styles.reloadButton}>Reload</Text>
              </TouchableOpacity>
              </View> 
          :
            !isLastCard  && <ActivityIndicator  size="large" color='black'></ActivityIndicator>
        }
        {
          isLastCard && <View> 
            <Text style={styles.noEvents}>No more Events to show</Text>  
              <TouchableOpacity onPress={()=>{setIsLastCard(false);getEvents(); getAttendanceForPhone()}} style={styles.reloadButtonContainer}>
                <Text style={styles.reloadButton}>Reload</Text>
              </TouchableOpacity>
            </View> 
        }
        </View>
        
        {
          !isLastCard && <View style={styles.button3Row}>
            {showGoing && 
            <TouchableOpacity 
              onPress={()=>{registerEventGoing()}}
              style={styles.button3}>
              <Text style={styles.going2}>going</Text>
            </TouchableOpacity>}

            {showBar ? <View style={styles.rect2}></View> : null}

              {showNot && <TouchableOpacity 
              onPress={()=>{registerEventNot()}}
              style={styles.button2}>
              <Text style={styles.not2}>not!</Text>
            </TouchableOpacity>}  
            
          </View>
        }
        <AdvertPanel></AdvertPanel>
    </View>
  );
}

//TODO: clean up unsued styles and rename to something more appropriate where needed
const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
    flexDirection: 'column',
    flex: 1,
    marginTop: 10,
    height: Dimensions.get('window').height-40
  },
  card: {
    flex: 1,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: "#E8E8E8",
    justifyContent: "center",
    backgroundColor: "white"
  },
  text: {
    textAlign: "center",
    fontSize: 50,
    backgroundColor: "transparent"
  },
  image1: {
    width: wp('15%'),
    height: hp('12%'),
    marginTop: hp('1%'),
    marginBottom: hp('4%'),    
    alignSelf: 'center'
  },
  button4: {
    alignSelf: 'center',
    alignItems: 'center',
    width: wp('40%'),
    height: hp('6%'),
    backgroundColor: "rgba(0,0,0,1)",
    elevation: 150,
    borderRadius: 19,
    borderColor: "#000000",
    borderWidth: 0,
    shadowOffset: {
      height: 5,
      width: 5
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowOpacity: 0.01,
    shadowRadius: 50,
    flexDirection: "row"
  },
  icon5Row: {
    flexDirection: "row",
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'space-around',
    // paddingHorizontal: wp('2%')
  },
   icon5: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('3%'), 
  },
  selectHere: {
    color: "rgba(255,255,255,1)",
    fontFamily: "Futura-Medium",
    alignSelf: "center",
    fontSize: hp('2%'), 
  },
  rect111:{
    shadowRadius: 20, shadowOpacity: 0.5 ,
    justifyContent: 'center',
    alignSelf: 'center',
    // marginLeft: 200,
    width: cardWidth,
    height: cardWidth,
    // backgroundColor: 'red',
    // height: hp('30%'),
    // backgroundColor: "grey",
    // marginRight: hp('2%'),
    // marginLeft: hp('2%'),
    borderRadius: 17,
    // marginBottom: hp('3%'),
    // marginTop: hp('2%'),
  },
  citySelector: {
    // position: 'absolute',
    // bottom: Dimensions.get('window').height/2,
    
   
  },
  button3Row: {
    flexDirection: "row",
    marginTop: -Dimensions.get('window').height/12,
    alignItems: 'center',
    alignSelf: 'center',
   
  },
  button3: {
    width: wp('30%'),
    height: hp('8%'),
    backgroundColor: "rgba(218,218,218,1)",
    borderRadius: 17,
    alignSelf: 'center',
  },
  reloadButtonContainer: {
    // width: wp('30%'),
    // height: hp('8%'),
    padding: 10,
    backgroundColor: "rgba(218,218,218,1)",
    borderRadius: 17,
    alignSelf: 'center',
  },
  button2: {
    width: wp('30%'),
    height: hp('8%'),
    backgroundColor: "rgba(218,218,218,1)",
    borderRadius: 17,
    alignSelf: 'center', 
 },
  going2: {
    color: "black",
    fontSize: hp('5%'),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    paddingTop: 6,
    alignSelf: 'center',
    flex: 1
  },
  reloadButton: {
    color: "black",
    fontSize: hp('3%'),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    
    alignSelf: 'center',
  },
  noEvents: {
    color: "black",
    fontSize: hp('2%'),
    fontFamily: "FuturaBT-Medium",
    textAlign: 'center',
  },
  not2: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('5%'),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    alignSelf: 'center',
    paddingTop: 6,
    flex: 1
  },
  
  rect6: {
    height: hp('10%'),
    backgroundColor: "rgba(230, 230, 230,1)",
    alignSelf: 'center',
    flex: 1,
    marginTop: hp('4%'),
    borderRadius: 7,
    marginTop: hp('2%'),
  },

  rect2: {
    width: wp('1%'),
    height: hp('4%'),
    // backgroundColor: "rgba(0,0,0,1)",
    borderLeftColor: "rgba(0,0,0,1)",
    borderLeftWidth: 1,
    alignSelf: 'center',
    alignItems: 'center',
    marginLeft: 3,
  },
  icon2Row: {
    flexDirection: "row",
    flex: 1,
    height: hp('9%'),
    alignSelf: 'center',
    justifyContent: 'space-around'
  }, 
  icons: {
    height: hp('9%'),
    flexDirection: "row",
    alignSelf: 'center',
    flex: 1,
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: hp('5%'),
  },
  icon3: {
    color: "rgba(0,0,0,1)",
    fontSize: hp('5%'),
  },
  icon6: {
    color: "rgba(0,0,0,1)",
    fontSize: hp('5%'),
  },
 
});

export default SelectLocation;
