import React, { Component } from "react";
import { StyleSheet, View, Image, Text, ScrollView } from "react-native";
import Svg, { Ellipse } from "react-native-svg";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

function Index(props) {
  return (<>
    <View style={styles.container}>
      {/* <ScrollView></ScrollView> */}
      <View style={styles.groupColumn}>
        <View style={styles.group}>
          <Image
            source={require("../assets/images/guide_logo.png")}
            resizeMode="contain"
            style={styles.image}
          ></Image>
          <Text style={styles.guide}>guide</Text>
        </View>
        
      </View>
      {/* <View style={styles.groupColumnFiller}></View> */}
    </View>
      <Text style={styles.poweredByHgGroup}>powered by hg group</Text></>

  );
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   alignItems: 'center',
   justifyContent: "center"
  },
  group: {
    // flex: 1,
  },
  image: {
    width: wp('50%'),
    height: hp('20%'),
    // alignSelf: 'center',
    marginTop: hp('4%'),
    // justifyContent: "center",
  },
  guide: {
    color: "#121212",
    fontSize: hp('10%'),
    fontFamily: "FuturaBT-Medium",
    // letterSpacing: -3,
    textAlign: 'center',
    // justifyContent: "center",

  },
  ellipse2: {
    width: 12,
    height: 12
  },
  ellipse1: {
    width: 12,
    height: 12,
    alignSelf: 'center'
  },
  ellipse2Row: {
    // height: 12,
    flexDirection: "row",
    // marginTop: hp('40%'),
    // marginLeft: 86,
    // marginRight: 78,   
    alignSelf: 'center'

  },
  groupColumn: {
    alignSelf: 'center',
    justifyContent: "center",
  },
  groupColumnFiller: {
    flex: 1
  },
  poweredByHgGroup: {
    color: "#121212",
    opacity: 0.17,
    fontSize: 20,
    fontFamily: "Futura-book",
    // marginBottom: 32,
    // marginLeft: 15,
    textAlign: 'center',
    marginBottom: 20,
  }
});

export default Index;
