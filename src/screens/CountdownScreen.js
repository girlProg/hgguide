import React, { Component, useState, useContext , useEffect} from "react";
import { StyleSheet, View, ScrollView, Text, Image, FlatList  } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Context} from '../context/eventsContext'
import CountdownItem from '../components/CountdownItem'

function CountdownScreen({navigation}) {
  const { state, getEvents, getStates, markAttendance, getAttendanceForPhone} = useContext(Context)
  useEffect (()=>{state.chosenState?  getEvents(state.chosenState) : getEvents() },[])
  const thumb = 'https://img.imageboss.me/width/100/quality:40/'

  return (
    <View style={styles.container}>

         <Text style={styles.january}>Countdown</Text>
        <FlatList 
          keyExtractor={item => item.id.toString()}
          data={state.events? state.events : []}
          renderItem={({ item,index }) => (
            // <Text>{item.days}</Text>
            <CountdownItem navigation={navigation} index={index} item={item} />
          )}
          />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image1: {
    width: wp('15%'),
    height: hp('12%'),
    marginTop: hp('1%'),
    marginBottom: hp('4%'),    
    alignSelf: 'center'
  },
  january: {
    textAlign: 'center',
      color: "rgba(0,0,0,1)",
      fontSize: 41,
      fontFamily: "Futura-Medium"
  },
  
});

export default CountdownScreen;
