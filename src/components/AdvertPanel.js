import React from 'react'
import { StyleSheet, Text, View, Dimensions, TouchableWithoutFeedback } from 'react-native'
import { useContext, useEffect } from 'react'
import { useState } from 'react';
import ProgressiveImage from './ProgressiveImage';
import {Context} from '../context/eventsContext'
import { Linking } from 'expo';

const AdvertPanel = ({advert}) => {
    const { state, getAdvert } = useContext(Context)
    const {height, width} = Dimensions.get('window')
    const thumb = 'https://img.imageboss.me/width/200/quality:40/'
    useEffect (()=>{ getAdvert() },[])


    return (
        <View style={{ shadowRadius: 20, shadowOpacity: 0.5 , elevation: 1, flex: 1, justifyContent: 'flex-end', }} onPress ={()=>Linking.openURL(state.advert.url)}>

            <View 
            style={{
              // marginTop: 10,
              // flex: 1,
              justifyContent: 'flex-end',
              // height: 60, 
              width: width,}}
          ></View>
          <TouchableWithoutFeedback style={{ shadowRadius: 20, shadowOpacity: 0.5 ,  justifyContent: 'flex-end', }} onPress ={()=>Linking.openURL(state.advert.url)}>
              <ProgressiveImage
                thumbnailSource={state.advert && { uri: thumb+state.advert.image }}
                style={{height: Dimensions.get('window').height/13, resizeMode: "cover", alignSelf: 'flex-end', }}
                source={state.advert && {uri: state.advert.image} } />
                </TouchableWithoutFeedback>
            </View>
            
    )
}

export default AdvertPanel

const styles = StyleSheet.create({})
