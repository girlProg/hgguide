import React from 'react'
import {Image} from 'react-native'

const LogoTitle = () => {
      return (
        <Image
            resizeMode = 'contain'
          source={require('../../src/assets/images/guide_logo.png')}
          style={{ width: 50, height: 50 }}
        />
      );
    }
  
export default LogoTitle
  