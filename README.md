
## hgguide mobile app 

This  is a mobile app for https://www.instagram.com/hg.guide/
* Find events happening in Nigeria.
* Made with react Native
* Backend Python Django 

* Master branch is expo
* bare barnch is react-native-cli

Packages are included as dependencies in package.json file

## Steps to run project

In the project directory, you can run:

### `yarn` or `npm install`

This will add the dependencies required to run the project.

### `yarn start` or `npm start`

This will start the project.
![Image of App UI](https://cdn2.yedi.com.ng/media/Images/files/hgguide.jpg)

for the xoxo
